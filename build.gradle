plugins {
   id 'de.fuerstenau.buildconfig' version '1.1.8'
}

apply plugin: 'java'
apply plugin: 'application'
apply plugin: 'idea'

mainClassName = 'io.finn.signald.Main'

compileJava.options.encoding = 'UTF-8'


repositories {
    maven {url "https://raw.github.com/AsamK/maven/master/releases/"}
    maven {url "https://plugins.gradle.org/m2/"}
    mavenCentral()
}

sourceSets {
    integrationTest {
        java {
            compileClasspath += main.output + test.output
            runtimeClasspath += main.output + test.output
            srcDir file('src/integration-tests/java')
        }
        resources.srcDir file('src/integration-tests/resources')
    }
}

configurations {
    integrationTestCompile.extendsFrom testCompile
    integrationTestRuntime.extendsFrom testRuntime
}

dependencies {
    compile 'com.github.turasa:signal-service-java:2.13.9_unofficial_1'
    compile 'org.bouncycastle:bcprov-jdk15on:1.65.01'
    compile 'com.kohlschutter.junixsocket:junixsocket-common:2.3.2'
    compile 'com.kohlschutter.junixsocket:junixsocket-native-common:2.3.2'
    compile 'org.apache.logging.log4j:log4j-api:2.13.3'
    compile 'org.apache.logging.log4j:log4j-core:2.13.3'
    compile 'io.sentry:sentry-log4j2:1.7.30'
    compile 'org.slf4j:slf4j-nop:1.8.0-beta4'
    compile 'info.picocli:picocli:4.3.2'
    testCompile 'org.junit.jupiter:junit-jupiter-api:5.3.1'
    testRuntime 'org.junit.jupiter:junit-jupiter-engine:5.3.1'
}


buildConfig {
  packageName = "io.finn.signald"
  version = System.getenv("VERSION") ?: "unversioned"
  appName = System.getenv("CI_PROJECT_NAME") ?: "signald"

  buildConfigField 'String', 'BRANCH', { System.getenv("CI_BUILD_REF_NAME") ?: "" }
  buildConfigField 'String', 'COMMIT', { System.getenv("CI_COMMIT_SHA") ?: "" }

  buildConfigField 'String', 'SIGNAL_URL', { System.getenv("SIGNAL_URL") ?: "https://textsecure-service.whispersystems.org" }
  buildConfigField 'String', 'SIGNAL_CDN_URL', { System.getenv("SIGNAL_CDN_URL") ?: "https://cdn.signal.org" }
  buildConfigField 'String', 'SIGNAL_CONTACT_DISCOVERY_URL', { System.getenv("SIGNAL_CONTACT_DISCOVERY_URL") ?: "https://cms.souqcdn.com" }
  buildConfigField 'String', 'USER_AGENT', { System.getenv("USER_AGENT") ?: "signald-" + version }
  buildConfigField "String", "UNIDENTIFIED_SENDER_TRUST_ROOT", "BXu6QIKVz5MA8gstzfOgRQGqyLqOwNKHL6INkv3IHWMF"
}


jar {
    manifest {
        attributes(
                'Implementation-Title': project.name,
                'Implementation-Version': project.version,
                'Main-Class': project.mainClassName,
        )
    }
}

// Find any 3rd party libraries which have released new versions
// to the central Maven repo since we last upgraded.
// http://daniel.gredler.net/2011/08/08/gradle-keeping-libraries-up-to-date/
task checkLibVersions {
    doLast {
        def checked = [:]
        allprojects {
            configurations.each { configuration ->
                configuration.allDependencies.each { dependency ->
                    def version = dependency.version
                    if (!checked[dependency]) {
                        def group = dependency.group
                        def path = group.replace('.', '/')
                        def name = dependency.name
                        def url = "https://repo1.maven.org/maven2/$path/$name/maven-metadata.xml"
                        try {
                            def metadata = new XmlSlurper().parseText(url.toURL().text)
                            // def versions = metadata.versioning.versions.version.collect { it.text() }
                            // versions.removeAll { it.toLowerCase().contains('alpha') }
                            // versions.removeAll { it.toLowerCase().contains('beta') }
                            // versions.removeAll { it.toLowerCase().contains('rc') }
                            // def newest = versions.max()
                            def newest = metadata.versioning.latest;
                            if ("$version" != "$newest") {
                                println "UPGRADE {\"group\": \"$group\", \"name\": \"$name\", \"current\": \"$version\", \"latest\": \"$newest\"}"
                            }
                        } catch (FileNotFoundException e) {
                            logger.debug "Unable to download $url: $e.message"
                        } catch (org.xml.sax.SAXParseException e) {
                            logger.debug "Unable to parse $url: $e.message"
                        }
                        checked[dependency] = true
                    }
                }
            }
        }
    }
}


task integrationTest(type: Test) {
    useJUnitPlatform()
    testClassesDirs = sourceSets.integrationTest.output.classesDirs
    classpath = sourceSets.integrationTest.runtimeClasspath
    outputs.upToDateWhen { false }
}
